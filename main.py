import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from scipy import stats
from astropy.cosmology import FlatLambdaCDM
from astropy import units as u
from astropy.coordinates import SkyCoord
from itertools import combinations

# Read the groups datafile with tab delimiter
gal_data = pd.read_csv('test/groups.tsv', delimiter='\t')

# Drop any rows that contain missing values
gal_data.dropna(inplace=True)

# Create dataframes for galaxies with LSB features and without them
gal_data_without_feat = gal_data[gal_data['features'] == 0]
gal_data_with_feat = gal_data[gal_data['features'] == 1]

# Find the average value of mean_mu
avg_mu_without_feat = gal_data_without_feat['mean_mu'].mean()
avg_mu_with_feat = gal_data_with_feat['mean_mu'].mean()

# Find the p values for the Shapiro-Wilk test
shapiro_pval_without_feat = stats.shapiro(gal_data_without_feat['mean_mu']).pvalue
shapiro_pval_with_feat = stats.shapiro(gal_data_with_feat['mean_mu']).pvalue

# Find the p value for the Fligner-Killeen test
fligner_pval = stats.fligner(gal_data_without_feat['mean_mu'], gal_data_with_feat['mean_mu']).pvalue

# Find the p value for the ANOVA test
anova_pval = stats.f_oneway(gal_data_without_feat['mean_mu'], gal_data_with_feat['mean_mu']).pvalue

# Read the galaxies_morphology and isolated_galaxies datafiles
grp_data = pd.read_csv('test/galaxies_morphology.tsv', delimiter='\t')
iso_data = pd.read_csv('test/isolated_galaxies.tsv', delimiter='\t')

# Plot the histograms for the Sersic index for both datasets
bin_width = 0.5
max_n = max(max(grp_data['n']), max(iso_data['n']))
bins = np.arange(0, max_n + bin_width, bin_width)
plt.hist(iso_data['n'], bins=bins, alpha=0.5, label='isolated galaxies', color='red', hatch='/', edgecolor='black')
plt.hist(grp_data['n'], bins=bins, alpha=0.5, label='groups galaxies', color='blue', hatch='\\', edgecolor='black')
plt.legend(loc='upper right')
plt.xlim(left=0)
plt.xlabel('n')
plt.ylabel('Count')
plt.grid()
plt.show()

# Calculate the fraction of galaxies with the Sersic index n > 2
iso_data_gt2 = iso_data.loc[iso_data['n'] > 2, 'n'].count() / iso_data['n'].count()
grp_data_gt2 = grp_data.loc[grp_data['n'] > 2, 'n'].count() / grp_data['n'].count()

# Find the p value for the two sample Kolmogorov-Smirnov test
ks_pval = stats.ks_2samp(iso_data['n'], grp_data['n']).pvalue

# Group the data from galaxies_morphology by the Group column
grp_agg = grp_data.groupby('Group')[['n', 'T']].mean().rename(columns={'n': 'mean_n', 'T': 'mean_T'})

# Merge the group aggregation data with the earlier galaxy data
merged_data = gal_data.merge(grp_agg, left_on='Group', right_index=True)

# Plot scatter plot for the following value-pairs: mean_mu - mean_n and mean_mu - mean_T
fig, axes = plt.subplots(nrows=1, ncols=2, sharey=True, squeeze=True)
plt.subplot(121)
plt.scatter(merged_data['mean_n'], merged_data['mean_mu'], c='r', s=5)
plt.xticks(np.arange(1, 9, 1))
plt.xlabel('n')
plt.ylabel('mu')
plt.subplot(122)
plt.scatter(merged_data['mean_T'], merged_data['mean_mu'], c='r', s=5)
plt.xticks(np.arange(-6, 7, 2))
plt.xlabel('T')
plt.show()

# Find the p values for the Shapiro-Wilk test
shapiro_pval_mean_mu = stats.shapiro(merged_data['mean_mu']).pvalue
shapiro_pval_mean_n = stats.shapiro(merged_data['mean_n']).pvalue
shapiro_pval_mean_T = stats.shapiro(merged_data['mean_T']).pvalue

# Find the p values for the Pearson correlation coefficients
pearson_pval_mu_n = stats.pearsonr(merged_data['mean_mu'], merged_data['mean_n']).pvalue
pearson_pval_mu_T = stats.pearsonr(merged_data['mean_mu'], merged_data['mean_T']).pvalue

# Initialize the cosmology model
cosmo_mdl = FlatLambdaCDM(H0=67.74, Om0=0.3089)

# Read the dataset with galaxies' equatorial coordinates
gal_coord = pd.read_csv('test/galaxies_coordinates.tsv', delimiter='\t')

# Loop through the different groups of galaxies
for gal in gal_data['Group']:

    # Calculate the index of galaxy data
    idx = gal_data[gal_data['Group'] == gal].index.max()

    # Calculate the angular distance in kiloparsecs for the z column
    z = gal_data[gal_data['Group'] == gal]['z'].max()
    ang_diam_dist = cosmo_mdl.angular_diameter_distance(z).to(u.kpc).value

    # Generate list of galaxies for the specified group
    gal_list = gal_coord[gal_coord['Group'] == gal].index.values.tolist()

    # Generate the list of combinations for each galaxy group
    gal_comb_list = list(combinations(gal_list, 2))

    # Create storage for the separation calculation
    sep_list = []

    # Loop through the galaxy combinations
    for comb in gal_comb_list:
        # Calculate the separation between two galaxies
        p0 = SkyCoord(
            ra=gal_coord['RA'][comb[0]] * u.degree,
            dec=gal_coord['DEC'][comb[0]] * u.degree,
            frame='fk5'
        )
        p1 = SkyCoord(
            ra=gal_coord['RA'][comb[1]] * u.degree,
            dec=gal_coord['DEC'][comb[1]] * u.degree,
            frame='fk5'
        )
        sep_list.append(p0.separation(p1).to(u.rad).value)

    # Calculate the median separation
    gal_data.at[idx, 'median_r'] = ang_diam_dist * np.median(sep_list)

# Plot a scatter plot for the median separation and the mean surface brightness
plt.scatter(gal_data['median_r'], gal_data['mean_mu'], c='r', s=5)
plt.xlabel('r')
plt.ylabel('mu')
plt.show()

# Find the p values for the Shapiro-Wilk test
shapiro_pval_gd_mean_mu = stats.shapiro(gal_data['mean_mu']).pvalue
shapiro_pval_gd_median_r = stats.shapiro(gal_data['median_r']).pvalue

# Find the p values for the Pearson correlation coefficients
pearson_pval_gd_mu_r = stats.pearsonr(gal_data['mean_mu'], gal_data['median_r']).pvalue

# Find the median separation for HCG 2
hcg2_median_r = gal_data[gal_data['Group'] == 'HCG 2']['median_r'].max()
